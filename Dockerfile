ARG CY_IMAGE
FROM $CY_IMAGE
LABEL maintainer="Abel Luck <abel@guardianproject.info>"

RUN set -ex ; \
  apt-get update ; \
  apt-get  install -y \
    curl \
    wget \
    postgresql-client \
    make \
    jq ; \
  rm -rf /var/lib/apt/lists/*
